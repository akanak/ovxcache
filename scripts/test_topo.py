#!/usr/bin/python

from mininet.net import Mininet
from mininet.topo import Topo
from mininet.log import lg, setLogLevel
from mininet.cli import CLI
from mininet.node import RemoteController
from mininet.link import TCLink

CORES = {
  'SEA': {'dpid': '000000000000010%s'}
  }

FANOUT = 6
    
class I2Topo(Topo):

  def __init__(self, enable_all = True):
    "Create Internet2 topology."

    # Add default members to class.
    super(I2Topo, self).__init__()

    # Add core switches
    self.cores = {}
    for switch in CORES:
      self.cores[switch] = self.addSwitch(switch, dpid=(CORES[switch]['dpid'] % '0'))

    # Add hosts and connect them to their core switch
    for switch in CORES:
      for count in xrange(1, FANOUT + 1):
        # Add hosts
        host = 'h_%s_%s' % (switch, count)
        ip = '10.0.0.%s' % count
        mac = CORES[switch]['dpid'][4:] % count
        h = self.addHost(host, ip=ip, mac=mac)
	linkopts = dict(bw=1000, delay="1ms", loss=0, use_htb=True)
        # Connect hosts to core switches
        self.addLink(h, self.cores[switch], **linkopts)
        #self.addLink(h, self.cores[switch])

    # Connect core switches
#    self.addLink(self.cores['SFO'], self.cores['SEA'])

if __name__ == '__main__':
   topo = I2Topo()
   ip = '127.0.0.1'
   port = 6633
#   port = 10000
   c = RemoteController('c', ip=ip, port=port)
   net = Mininet(topo=topo, link=TCLink, autoSetMacs=True, xterms=False, controller=None)
   #net = Mininet(topo=topo, autoSetMacs=True, xterms=False, controller=None)
   net.addController(c)
   net.start()
   print "Hosts configured with IPs, switches pointing to OpenVirteX at %s:%s" % (ip, port)
   CLI(net)
   net.stop()

