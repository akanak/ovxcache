# Creating three tenant IDs.
#python ovxctl.py -n createNetwork tcp:localhost:10000 10.0.0.0 16
python ovxctl.py -n createNetwork tcp:localhost:10000 192.168.0.0 16
#python ovxctl.py -n createNetwork tcp:localhost:20000 20.0.0.0 16
#python ovxctl.py -n createNetwork tcp:localhost:30000 30.0.0.0 16

# Creating switch in tenant ID 1
python ovxctl.py -n createSwitch 1 00:00:00:00:00:00:00:01

# Creating switch in tenant ID 2
#python ovxctl.py -n createSwitch 2 00:00:00:00:00:00:00:01

# Creating virtual ports on the  switch 1
python ovxctl.py -n createPort 1 00:00:00:00:00:00:00:01 1
python ovxctl.py -n createPort 1 00:00:00:00:00:00:00:01 2
python ovxctl.py -n createPort 1 00:00:00:00:00:00:00:01 3
python ovxctl.py -n createPort 1 00:00:00:00:00:00:00:01 4
#python ovxctl.py -n createPort 2 00:00:00:00:00:00:00:01 5
#python ovxctl.py -n createPort 2 00:00:00:00:00:00:00:01 6

# Connect the hosts to the switches
python ovxctl.py -n connectHost 1 00:a4:23:05:00:00:00:01 1 00:00:00:00:00:01
python ovxctl.py -n connectHost 1 00:a4:23:05:00:00:00:01 2 00:01:00:00:00:01
python ovxctl.py -n connectHost 1 00:a4:23:05:00:00:00:01 3 00:02:00:00:00:01
python ovxctl.py -n connectHost 1 00:a4:23:05:00:00:00:01 4 00:03:00:00:00:01
#python ovxctl.py -n connectHost 1 00:a4:23:05:00:00:00:01 5 00:04:00:00:01:01
#python ovxctl.py -n connectHost 1 00:a4:23:05:00:00:00:01 6 00:05:00:00:01:01
#python ovxctl.py -n connectHost 2 00:a4:23:05:00:00:00:01 1 00:00:00:00:01:05
#python ovxctl.py -n connectHost 2 00:a4:23:05:00:00:00:01 2 00:00:00:00:01:06

# Start virtual network
python ovxctl.py -n startNetwork 1
#python ovxctl.py -n startNetwork 2
