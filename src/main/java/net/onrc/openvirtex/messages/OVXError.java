/*******************************************************************************
 * Copyright 2014 Open Networking Laboratory
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 ******************************************************************************/
package net.onrc.openvirtex.messages;

import java.util.Map;

import net.onrc.openvirtex.elements.datapath.OVXSwitch;
import net.onrc.openvirtex.elements.datapath.PhysicalSwitch;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.openflow.protocol.OFError;
import org.openflow.protocol.OFFlowMod;
import org.openflow.protocol.OFPort;
import org.openflow.protocol.OFType;

public class OVXError extends OFError implements Virtualizable, Devirtualizable {

    private final static Logger log = LogManager.getLogger(OVXError.class.getName());

    @Override
    public void devirtualize(final OVXSwitch sw) {
        // TODO Auto-generated method stub

    }

    @Override
    public void virtualize(final PhysicalSwitch sw) {    	
    	/*
         * TODO: For now, just report the error. In the future parse them and
         * forward to controller if need be.
         */    	
    	log.debug("Received Error from sw {}: {}", sw.getName(), getErrorString(this, sw) + "  xid = " + this.getXid());
        log.error(getErrorString(this, sw) + "    xid = " + this.getXid());
        
        int etint = 0xffff & this.getErrorType();
        OFErrorType et = OFErrorType.values()[etint];
        switch(et) {
        case OFPET_FLOW_MOD_FAILED:
            OFFlowModFailedCode fmfc = OFFlowModFailedCode.values()[0xffff & this
                                                                    .getErrorCode()];
            if (fmfc == OFFlowModFailedCode.OFPFMFC_ALL_TABLES_FULL) {
            	handleTableFullError(this, sw);
            }
            break;
        default:
        	break;
        }
        

    }
    
    private static void handleTableFullError(OFError error, final PhysicalSwitch sw) {
        int tid = error.getXid() >> 16;
        OVXFlowMod tmpMod = sw.deleteMinFlowModFromCache(error.getXid());
        if (tmpMod == null) {
        	log.debug("ANJALI TEST: ********* Something wrong******************");
        	return;
        }
        
        OVXFlowMod repMod = tmpMod.clone();
        repMod.setOutPort(OFPort.OFPP_MAX);
        repMod.setType(OFType.FLOW_MOD);
        repMod.setCommand(OFFlowMod.OFPFC_DELETE);
        repMod.setFlags((short)OFFlowMod.OFPFF_SEND_FLOW_REM);
        
        log.debug("ANJALI TEST: Deleting entry : " + repMod + " from the switch" + sw.getName());
        sw.sendMsg(repMod, null);
        
        // Also re-send the original FlowMod message with action set to ADD
    	OVXFlowMod msg = sw.getFlowModEntry(error.getXid());
    	if (msg != null) {
            log.debug("ANJALI TEST: Found xid= " + msg.getXid() + 
            		"! Re-sending " + msg + " to sw " + sw.getName());
              sw.sendMsg(msg, null);
    	}
    }
    

    /**
     * Get an error string from the OFError.
     *
     * @param error the OpenFlow error
     * @return the error string
     */
    private static String getErrorString(OFError error, final PhysicalSwitch sw) {
        // TODO: this really should be OFError.toString. Sigh.
        int etint = 0xffff & error.getErrorType();
        if (etint < 0 || etint >= OFErrorType.values().length) {
            return String.format("Unknown error type %d", etint);
        }
        OFErrorType et = OFErrorType.values()[etint];
        switch (et) {
        case OFPET_HELLO_FAILED:
            OFHelloFailedCode hfc = OFHelloFailedCode.values()[0xffff & error
                    .getErrorCode()];
            return String.format("Error %s %s", et, hfc);
        case OFPET_BAD_REQUEST:
            OFBadRequestCode brc = OFBadRequestCode.values()[0xffff & error
                    .getErrorCode()];
            return String.format("Error %s %s", et, brc);
        case OFPET_BAD_ACTION:
            OFBadActionCode bac = OFBadActionCode.values()[0xffff & error
                    .getErrorCode()];
            return String.format("Error %s %s", et, bac);
        case OFPET_FLOW_MOD_FAILED:
            OFFlowModFailedCode fmfc = OFFlowModFailedCode.values()[0xffff & error
                    .getErrorCode()];
            return String.format("Error %s %s", et, fmfc);
        case OFPET_PORT_MOD_FAILED:
            OFPortModFailedCode pmfc = OFPortModFailedCode.values()[0xffff & error
                    .getErrorCode()];
            return String.format("Error %s %s", et, pmfc);
        case OFPET_QUEUE_OP_FAILED:
            OFQueueOpFailedCode qofc = OFQueueOpFailedCode.values()[0xffff & error
                    .getErrorCode()];
            return String.format("Error %s %s", et, qofc);
        case OFPET_VENDOR_ERROR:
            // no codes known for vendor error
            return String.format("Error %s", et);
        default:
            break;
        }
        return null;
    }

}
