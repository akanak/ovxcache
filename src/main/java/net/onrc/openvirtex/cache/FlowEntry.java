package net.onrc.openvirtex.cache;
import java.util.Date;


public class FlowEntry {
	
	private long entry_time;
	private long timeout;
	private long cookie;
	
	FlowEntry(long cookie, long timeout) {
		this.entry_time = new Date().getTime();
		this.timeout = timeout;
		this.cookie = cookie;
	}

	public long getEntry_time() {
		return entry_time;
	}

	public void setEntry_time(long entry_time) {
		this.entry_time = entry_time;
	}

	public long getTimeout() {
		return timeout;
	}

	public void setTimeout(long timeout) {
		this.timeout = timeout;
	}

	public long getCookie() {
		return cookie;
	}

	public void setCookie(long cookie) {
		this.cookie = cookie;
	}
	
}
