package net.onrc.openvirtex.cache;
import java.util.Comparator;

public class FlowComparator implements Comparator<FlowEntry>
{
	 @Override
	 public int compare(FlowEntry x, FlowEntry y) {
		 long exp1 = x.getEntry_time() + x.getTimeout();
		 long exp2 = y.getEntry_time() + y.getTimeout();
	     if (exp1 < exp2) {
	         return -1;
	     } else if (exp1 > exp2) {
	         return 1;
	     } else {
	    	 return 0;
	     }
	 }
}