package net.onrc.openvirtex.cache;
import java.util.Date;
import java.util.Iterator;
import java.util.Map;
import java.util.PriorityQueue;
import java.util.concurrent.ConcurrentHashMap;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.openflow.protocol.OFMatch;

import net.onrc.openvirtex.messages.OVXFlowMod;

public class FlowCache {
	public enum flow_tag { CACHE_NONE, CACHE_ONLY, CACHE_REP, REM_PENDING };
	
	private static Logger log = LogManager.getLogger(FlowCache.class.getName());

	/* Xid to Coookie, used to match Error with cached flow */
	private ConcurrentHashMap<Integer, Long> xidMap;
	
	/* Flows segregated by category, each queue sorted by time to expire */	
	private ConcurrentHashMap<flow_tag, PriorityQueue<FlowEntry>> tagMap;
	private ConcurrentHashMap<Long,OVXFlowMod> flowModMap;
	
	public FlowCache() {     
	     xidMap = new ConcurrentHashMap<Integer, Long>();
	     flowModMap = new ConcurrentHashMap<Long,OVXFlowMod>();
	     tagMap = new ConcurrentHashMap<flow_tag, PriorityQueue<FlowEntry>>();
	     tagMap.put(flow_tag.CACHE_ONLY, new PriorityQueue<FlowEntry>(10, new FlowComparator()));
	     tagMap.put(flow_tag.CACHE_REP, new PriorityQueue<FlowEntry>(10, new FlowComparator()));
	     tagMap.put(flow_tag.REM_PENDING, new PriorityQueue<FlowEntry>(10, new FlowComparator()));
	}
	
	public void addCacheEntry(OVXFlowMod flow) {
		log.debug("ANJALI TEST: Adding entry " + flow.getCookie());
		Long cookie = flow.getCookie();
		this.xidMap.put(flow.getXid(), cookie);
		long timeout = flow.getIdleTimeout();
		this.tagMap.get(flow_tag.CACHE_REP).add(new FlowEntry(cookie, timeout));
		this.flowModMap.put(cookie, flow);
	}
	
	public OVXFlowMod getCacheEntryByXid(Integer xid) {
		return this.flowModMap.get(xidMap.get(xid));
	}
	
	public OVXFlowMod getCacheEntryByCookie(Long cookie) {
		return this.flowModMap.get(cookie);
	}
	
	public OVXFlowMod delMinCacheEntry(int xid) {
		log.debug("ANJALI TEST: In delMinCacheEntry");
		dumpCache();
		if (this.tagMap.isEmpty()) {
			log.debug("ANJALI TEST: tagMap empty");
			return null;
		}
		
		FlowEntry entry = this.tagMap.get(flow_tag.CACHE_REP).peek();
		if (entry == null) {
			log.debug("ANJALI TEST: CACHE_REP empty");
			return null;
		}
		
		if (!this.flowModMap.containsKey(entry.getCookie())) {
			// We should find the entry since we found a valid cache cookie!
			log.debug("ANJALI TEST: No matching rule entry, cookie = " + entry.getCookie());
			return null;
		}
		
		// Remove it from the tagMap, we anyways need to add it to REM_PENDING
		// later.
		this.tagMap.get(flow_tag.CACHE_REP).remove();

		// Create a new flowMod
		FlowEntry retEntry = null;
		OVXFlowMod tmpMod = new OVXFlowMod();
		tmpMod = this.flowModMap.get(entry.getCookie()).clone();
		if (tmpMod.getXid() == xid) {
			log.debug("Skipping xid = " + xid);
			retEntry = this.tagMap.get(flow_tag.CACHE_REP).peek();
			if (retEntry == null) {
				log.debug("No more entries found");
				// Nothing was found, add the removed entry back before returning.
				this.tagMap.get(flow_tag.CACHE_REP).add(entry);
				return null;
			}			
			tmpMod = this.flowModMap.get(retEntry.getCookie());
			this.tagMap.get(flow_tag.CACHE_REP).remove();
			this.tagMap.get(flow_tag.REM_PENDING).add(retEntry);
			this.tagMap.get(flow_tag.CACHE_REP).add(entry);
			return tmpMod;
		}
		
		log.debug("ANJALI TEST: Deleting entry " + tmpMod.getCookie());
		// Add it to REM_PENDING		
		this.tagMap.get(flow_tag.REM_PENDING).add(entry);
		return tmpMod;
	}
	
	public void dumpCache() {
		log.info("ANJALI TEST: Dumping Cache *********************************************"); 
		for (Map.Entry<Long, OVXFlowMod> entry : this.flowModMap.entrySet()) {
			log.info("Cookie = " + entry.getKey() + ",  FlowEntry = " + entry.getValue());
		}
		log.info("ANJALI TEST: End of Cache *********************************************");
	}

	public void delCacheEntryByCookie(long cookie) {
		FlowEntry entry = null;
		PriorityQueue<FlowEntry> pq = this.tagMap.get(flow_tag.REM_PENDING);
		Iterator<FlowEntry> it = pq.iterator();
		while (it.hasNext()) {
			entry = it.next();
			if (entry.getCookie() == cookie) {
				log.debug("Found entry in REM_pending, FlowRemoved received, hence moving it to cache only");
				break;
			}
		}
		
		if (entry != null) {
			pq.remove(entry);
			this.tagMap.get(flow_tag.CACHE_ONLY).add(entry);
		}
		

//		for (Map.Entry<Integer, Long> mapEntry : this.xidMap.entrySet()) {
//			if (mapEntry.getValue() == cookie) {
//				this.xidMap.remove(mapEntry);
//			}
//		}

		// OVXFlowRemoved received? remove from the cache
		// this.flowModMap.remove(cookie);
	}

	public OVXFlowMod getMatchingFlowEntry(OFMatch match) {
		boolean match_found = false;
		log.debug("ANJALI_TEST :  Packetin match field = " + match.toString());
		dumpCache();
		// Look into CACHE_ONLY flows.
		PriorityQueue<FlowEntry> pq = this.tagMap.get(flow_tag.CACHE_ONLY);
		Iterator<FlowEntry> it = pq.iterator();
		FlowEntry entry = null;
		OVXFlowMod tmpMod = null;
		while (it.hasNext()) {
			entry = it.next();
			tmpMod = this.flowModMap.get(entry.getCookie());
			if (tmpMod.getMatch().getInputPort() == match.getInputPort() &&
					tmpMod.getMatch().getNetworkDestination() == match.getNetworkDestination() &&
					tmpMod.getMatch().getNetworkSource() == match.getNetworkSource()) {
				log.debug("ANJALI TEST: Matching flow entry found in cache");
				log.debug("ANJALI_TEST :  FlowMod match field = " + tmpMod.getMatch().toString());
				match_found = true;
				break;
			}
		}
		if (match_found == true) {
		   return tmpMod;
		} 
		return null;
	}
	
	public void moveCacheEntry(flow_tag from, flow_tag to, long cookie) {
		
		if ((from == flow_tag.CACHE_ONLY) && (to == flow_tag.CACHE_REP)) {
			/* PacketIn processing */
			FlowEntry entry = null;
			PriorityQueue<FlowEntry> pq = this.tagMap.get(from);
			Iterator<FlowEntry> it = pq.iterator();
			while (it.hasNext()) {
				entry = it.next();
				if (entry.getCookie() == cookie) {
					break;
				}
			}
			
			if (entry != null){
				pq.remove(entry);
				entry.setEntry_time(new Date().getTime());
				this.tagMap.get(to).add(entry);
			}
		}
		
	}
}
